#!/bin/bash

set -e

apt="apt-get -qqy"

if [[ -n "${DEBUG}" ]]; then
    set -x
    apt="apt-get -y"
fi

script_dir="$(dirname "$(readlink -f "$0")")"
export DEBIAN_FRONTEND=noninteractive

. /etc/lsb-release

printf "\e[1;36mInstalling toolchain #1 ...\e[0m\n"
$apt update
$apt install \
    jq \
    curl

printf "\e[1;36mFetching upstream version ...\e[0m"
upstream="$(curl -s "https://sources.debian.org/api/src/zfs-linux/?suite=${!DISTRIB_CODENAME:-sid}" | jq -re '.versions[0].version')"
printf "\e[0;32m %s\e[0m\n" "${upstream}"
zfs_linux="${upstream%%-*}"

if [ "${FORCE}" == "true" ]; then
    current="0"
else
    printf "\e[1;36mFetching current repo version ...\e[0m"
    current=$(curl -sfkL "https://packaging.gitlab.io/zfs-linux/${DISTRIB_CODENAME}/.version" || echo "0")
fi
printf "\e[0;32m %s\e[0m\n" "${current}"

uid="$(stat -c %u "${script_dir}"/build.sh)"
gid="$(stat -c %g "${script_dir}"/build.sh)"
install -d -o "${uid}" -g "${gid}" "${script_dir}/${CI_JOB_NAME:-${DISTRIB_CODENAME}}"
tmpdir="$(mktemp -d)"
cd "${tmpdir}"

if dpkg --compare-versions "${upstream}" gt "${current}"; then
    printf "\e[1;36mBuilding new version ...\e[0m\n"
    printf "\e[1;36mInstalling toolchain #2 ...\e[0m\n"
    $apt update
    $apt install \
        ccache \
        devscripts \
        equivs \
        libtirpc-dev
    export PATH="/usr/lib/ccache/bin/:$PATH"
    export CCACHE_DIR="${CCACHE_DIR:-${CI_PROJECT_DIR:-/tmp}/ccache}"
    mkdir -p "$CCACHE_DIR"
    if [ -f "${script_dir}/.build/${DISTRIB_CODENAME}.sh" ]; then
        "${script_dir}/.build/${DISTRIB_CODENAME}.sh"
    fi
    for file in "zfs-linux_${upstream}.dsc" "zfs-linux_${zfs_linux}.orig.tar.gz" "zfs-linux_${upstream}.debian.tar.xz"
    do
        curl -sLO "http://deb.debian.org/debian/pool/contrib/z/zfs-linux/${file}"
    done
    dpkg-source -x "zfs-linux_${upstream}.dsc"
    mk-build-deps "zfs-linux-${zfs_linux}/debian/control"
    $apt install "./zfs-linux-build-deps_${upstream}_all.deb"
    rm -f "./zfs-linux-build-deps_${upstream}_all.deb"
    cd "zfs-linux-${zfs_linux}"
    rm -f ./debian/*.symbols
    debuild \
        --preserve-envvar=CCACHE_DIR \
        --prepend-path=/usr/lib/ccache \
        --no-lintian \
        -us -uc \
        -j"$(getconf _NPROCESSORS_ONLN)"
    echo "${upstream}" > "${tmpdir}"/.version
    install -o "${uid}" -g "${gid}" -m 644 "${tmpdir}"/.version "${script_dir}/${CI_JOB_NAME:-${DISTRIB_CODENAME}}/"
fi

arch="$(dpkg --print-architecture)"
if ! command -V nfpm >/dev/null 2>&1; then
    nfpm_version="${NFPM_VERSION:-2.39.0}"
    curl -sfLo /tmp/nfpm.deb "https://github.com/goreleaser/nfpm/releases/download/v${nfpm_version}/nfpm_${nfpm_version}_${arch}.deb"
    apt-get -qqy install /tmp/nfpm.deb
fi

cat >/tmp/zfs-dkms-ccache.yaml <<EOF
name: zfs-dkms-ccache
version: 2024.9.0
arch: all
section: kernel
version_schema: none
description: speed up subsequent zfs-dkms builds using ccache
maintainer: morph027 <morph@morph027.de>
vendor: morph027
homepage: https://gitlab.com/packaging/zfs-linux
contents:
  - dst: /var/cache/dkms/zfs
    type: dir
    file_info:
      mode: 0755
  - src: ${script_dir}/.packaging/etc/dkms/zfs.conf
    dst: /etc/dkms/zfs.conf
depends:
  - zfs-dkms
  - ccache
EOF
nfpm package --config /tmp/zfs-dkms-ccache.yaml --packager deb
install -o "${uid}" -g "${gid}" -m 644 "${tmpdir}"/*.deb "${script_dir}/${CI_JOB_NAME:-${DISTRIB_CODENAME}}/"
