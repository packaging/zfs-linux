# Debian [zfs-linux unstable](https://packages.debian.org/search?suite=sid&searchon=names&keywords=zfs-dkms) backport packages for Ubuntu

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/zfs-linux.asc https://packaging.gitlab.io/zfs-linux/gpg.key
```

## Add repo to apt

```bash
. /etc/lsb-release; echo "deb [arch=amd64] https://packaging.gitlab.io/zfs-linux/${DISTRIB_CODENAME} ${DISTRIB_CODENAME}-zfs-linux-backport main" | sudo tee /etc/apt/sources.list.d/zfs-linux.list
```

## Goodies

### `zfs-dkms-ccache`

This package adds `ccache` to `zfs-dkms` builds, so subsequent builds are going to be faster.
