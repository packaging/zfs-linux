#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

dists=("noble")

for dist in "${dists[@]}"
do
    for dir in "${CI_PROJECT_DIR}"/"${dist}"-*; do
        export SCAN_DIR="${dir}"
        export CODENAME="${dist}-zfs-linux-backport"
        /deb.sh "${dist}"
    done;
done
